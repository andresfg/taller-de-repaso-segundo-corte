/*1. En una tienda se venden artículos de primera necesidad, a los cuales se les
aplica un descuento del 20%, de la compra total, si esta es igual o mayor a $50.
Escriba un algoritmo, que a partir del importe total de la compra muestre lo que
debe pagar el cliente.*/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	float precio, precioFinal, descuento=0.8;
	
	cout<<"\t\t\tTIENDA DE CHUCHO\n\n";

	cout<<"Precio total de la compra: ";
    cin>>precio;
	cout<<"\n";

    if (precio >=50)
    {
        precioFinal = precio*descuento;
    }
    else
    {
        precioFinal = precio;
    }
        
    cout<<"El precio a pagar es de "<<precioFinal;

	
	getch();
	return 0;
}
