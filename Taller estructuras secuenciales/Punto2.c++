/*2. Se desea saber si un número es par o impar. Diseñe un Algoritmo en el cual el
usuario, ingrese el número y el programa muestre con un mensaje, si éste es
par o no.*/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	int num;
	
	cout<<"Digite un numero entero: ";
	cin>>num;
	if (num%2==0)
	{
		cout<<"El numero es par";
	}
	
	if (num%2!=0)
	{
		cout<<"El numero es impar";
	}
	
	getch();
	return 0;
}
