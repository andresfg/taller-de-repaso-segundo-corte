/*Diseñe un programa, que, dada la nota de alumno, imprima en la pantalla un
comentario sobre esa nota. El criterio para los comentarios es el siguiente:
a. Si nota es mayor o igual a 9 “Excelente”
b. Si nota es mayor o igual a 8 “Muy Bueno”
c. Si nota es mayor o igual a 7 “Bueno”
d. Si nota es mayor o igual a 6 “Regular”
e. Si nota es menor que 6 “Necesita Mejorar”*/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	float Nota;
	
	cout<<"\t\t\tNOTAS DE ESTUDIANTES\n\n";

    cout<<"Ingrese la nota del estudiante: ";
    cin>>Nota;

    cout<<"\n";

    if (Nota>=9)
    {
        cout<<"Excelente!";
    }
    else if((Nota>=8) && (Nota<9))
    {
        cout<<"Muy bueno";
    }
    else if((Nota>=7) && (Nota<8))
    {
        cout<<"Bueno";
    }    
    else if((Nota>=6) && (Nota<7))
    {
        cout<<"Regular";
    }
    else if (Nota<6)
    {
        cout<<"Necesitas mejorar";
    }
        
	
	
	getch();
	return 0;
}
