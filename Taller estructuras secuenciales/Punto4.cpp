/*4. Dada el peso, la altura y el sexo, de unos estudiantes. Determinar la cantidad de
vitaminas que deben consumir estos estudiantes, en base al siguiente criterio:
 Si son varones, y su estatura es mayor a 1.60, y su peso es mayor o igual a
150 lb, su dosis, serán: 20% de la estatura y 80% de su peso. De lo contrario,
la dosis será la siguiente: 30% de la estatura y 70% de su peso.
 Si son mujeres, y su estatura es mayor de a 1.50 m y su peso es mayor o
igual a 130 lb, su dosis será: 25% de la estatura y 75% de su peso. De lo
contrario, la dosis será: 35% de la estatura y 65% de su peso. La dosis debe
ser expresada en gramos.*/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	int sexo;
	float altura,peso,VIT,vitaminA,vitaminP;

	system("color 0f");
	
	system("cls");
	cout<<"\t\t\t\t\t\tBIENVENIDO!\n\n\n";

    cout<<"Escoga el sexo.  [1. Masculino    -    2.Femenino]: ";
	cin>>sexo;
	cout<<"\n";
	cout<<"Ingrese su altura(metros): ";
	cin>>altura;
	cout<<"\n";
	cout<<"Ingrese su peso(lb): ";
	cin>>peso;


	cout<<"\n\n";

    switch(sexo)
    {
    case 1:
    if((altura>1.6)&&(peso>=150))
	{
		vitaminA=(altura*0.2);
		vitaminP=(peso*0.8);
        VIT=vitaminA+vitaminP;
		cout<<"La cantidad de vitaminas es de "<<VIT<<" g";
	}
    else
    {
        vitaminA=(altura*0.3);
		vitaminP=(peso*0.7);
        VIT=vitaminA+vitaminP;
		cout<<"La cantidad de vitaminas es de "<<VIT<<" g";
    }
    break;

	case 2:
	if((altura>1.5)&&(peso>=130))
	{
		vitaminA=(altura*0.25);
		vitaminP=(peso*0.75);
        VIT=vitaminA+vitaminP;
		cout<<"La cantidad de vitaminas es de "<<VIT<<" g";
	}
    else
    {
        vitaminA=(altura*0.35);
		vitaminP=(peso*0.65);
        VIT=vitaminA+vitaminP;
		cout<<"La cantidad de vitaminas es de "<<VIT<<" g";
    }
	

	break;
	}
    
	
	system("pause");
	return 0;
}
