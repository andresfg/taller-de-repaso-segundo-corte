/*2. Hacer un algoritmo que calcule el total a pagar por la compra
de camisas. Sí se compran tres camisas o más se aplica un descuento
del 20% sobre el total de la compra y sí son menos de tres concederán
un descuento del 10%.*/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	int opcion,camisas;
	float descuento,precio,total,precio_final;
    string porcentaje;
	
	system("cls");

    cout<<"\t\t\t\t\tTIENDA DE ROPA\n\n\n";

	cout<<"Numero de camisas a comprar: ";
    cin>>camisas;
    cout<<"Precio de las camisas: ";
    cin>>precio;

    total=camisas*precio;

    if(camisas>=3)
    {
        porcentaje="20%";
        descuento=total*0.2;
        precio_final=total-descuento;
    }
    else
    {
        porcentaje="10%";
        descuento=total*0.1;
        precio_final=total-descuento;
    }
    
	cout<<"\n\n";
    cout<<"El valor a pagar es de "<<precio_final<<" y se realizo un descuento del "<<porcentaje<<"\nRestandole "<<descuento<<" al precio"<<endl;
	
	system("pause");
	return 0;
}
