/*En un almacén se hace un 20% de descuento a los clientes cuya compra supere los $100.000
¿Cuál será la cantidad que pagará una persona por su compra?*/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	float precio, precioFinal, descuento=0.8;
	
	cout<<"\t\t\tALMACEN\n\n";

	cout<<"Ingresar el precio de la compra: ";
    cin>>precio;
	cout<<"\n";

    if (precio > 100000)
    {
        precioFinal = precio*descuento;
    }
    else
    {
        precioFinal = precio;
    }
        
    cout<<"El precio a pagar es de "<<precioFinal;

	
	getch();
	
	getch();
	return 0;
}
