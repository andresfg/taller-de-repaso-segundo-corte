/*3. Calcular el total que una persona debe pagar en un almacén de
llantas, sí el precio de cada llanta es de $80.000 sí se compran menos
de 5 llantas y de $70.000 sí se compran 5 o más.*/


#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	int llantas;
	float precio,precio_final;
	
    system("cls");
	cout<<"\t\t\t\t\tALMACEN DE LLANTAS\n\n\n";

	cout<<"Numero de llantas a comprar:";
    cin>>llantas;
    

    if (llantas>=5)
    {
        precio=7;
    }
    else if ((llantas>0)&&(llantas<5))
    {
        precio=8;
    }
    
    precio_final=precio*llantas;
    
    if (llantas>1)
    {
        cout<<"\n\nEl precio por las "<<llantas<<" llantas es de "<<"$"<<precio_final<<"0.000";
    }
    else if (llantas==1)
	{
        cout<<"\n\nEl precio por la "<<llantas<<" llanta es de "<<"$"<<precio_final<<"0.000";
    }

	getch();
	return 0;
}
