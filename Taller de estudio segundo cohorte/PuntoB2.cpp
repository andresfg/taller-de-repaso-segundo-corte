//2. Un programa que calcule la suma de primeros 100 términos de la serie FIBONACCI.

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	int opcion;
    double n=100,x=0,y=1,z=1,sum=0;

    do
    {
        system("color 0f");
        cout<<"\t\t\t\t\t\t\tSERIE FIBONACCI\n\n";
        cout<<"\tQue desea hacer?: \n";
        cout<<"(1) Mostrar serie  -  (2) Sumar serie\n";
        cin>>opcion;

        switch (opcion)
        {
        case 1:
            cout<<"1";
            for (int i=1;i<n;i++)
            {
                z = x + y;
                cout<<", "<<z;
                x = y;
                y = z;  
            }        
            break;
    
        case 2:
            for (int i=1;i<n;i++)
            {
                z = x + y;
                x = y;
                y = z;
                sum+=z;
                cout<<"El resultado de la adición de  la serie es "<<sum;  
            }    
        
            break;    
    
        default:
            system("cls");
            system("color 4");
            cout<<"\nERROR - Ingrese una opcion valida\n\n";
            system("pause");
            system("cls");
            break;
        }

    } while ((opcion!=1)&&(opcion!=2));
    


	getch();
	return 0;
}
