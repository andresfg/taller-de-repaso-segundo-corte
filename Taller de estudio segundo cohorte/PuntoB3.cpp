/* 3. Realizar un programa en C que lea los nombres y las 3 notas parciales de
35 estudiantes. Se debe calcular el promedio por cada estudiante y
clasificarlo en regular, bueno y excelente de acuerdo a la nota final, calcular
e imprimir cuantos estudiantes ganaron, cuantos perdieron, el porcentaje de
los que ganaron y el porcentaje de los que perdieron, el porcentaje de
regulares, de buenos y excelentes
NOTA FINAL CATEGORIA
Mayor de 3 y menor de 4 Regular
Mayor de 4 y menor de 4.5 Bueno
Mayor de 4.5 y menor de 5 Excelente*/


#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main(){

    string Nombre;
    float promedio,n1,n2,n3,porcentaje_ganados,porcentaje_perdidos;
    int Numdestudiantes=35,conteo=0,perdidos,opcion,num=1;

    do
    {
        system("cls");
        cout<<"\t\t\t\t\t\t\tCALCULADOR DE NOTAS\n\n";

        for (int i=1;i<=Numdestudiantes;i++)
        {
            cout<<"\n"<<num++;
            cout<<". Nombre del estudiante: ";
            cin>>Nombre;
            
            cout<<"Notas del estudiante: \n";
            cin>>n1;
            cin>>n2;
            cin>>n3;

            promedio = (n1+n2+n3)/3;

            cout<<"El promedio es: "<<promedio;

            if (promedio<3)
            {
                cout<<". Perdio la materia";
                cout<<"\n";
            }
            else if((promedio>=3)&&(promedio<4))
            {
                cout<<". Promedio regular";
                conteo++;
                cout<<"\n";
            }
            else if((promedio>=4)&&(promedio<4.5))
            {
                cout<<". Promedio bueno";
                conteo++;
                cout<<"\n";
            }
            else if ((promedio>=4.5)&&(promedio<5))
            {
                cout<<". Promedio excelente";
                conteo++;
                cout<<"\n";
            }

        

        } 

        perdidos=Numdestudiantes-conteo;
        porcentaje_ganados=conteo*100/Numdestudiantes;
        porcentaje_perdidos=100-porcentaje_ganados;
    
        cout<<"\n\n";
        cout<<"El numero de estudiantes que ganaron la materia fue "<<conteo<<endl;
        cout<<"El numero de estudiantes que perdieron la materia fue "<<perdidos<<endl;
        cout<<"El "<<porcentaje_ganados<<"% de los estudiantes ganaron la materia"<<endl;
        cout<<"El "<<porcentaje_perdidos<<"% de los estudiantes perdieron la materia"<<endl;

        cout<<"\n\n";
        cout<<"Desea calcular mas notas?\n";
        cout<<"Si.....................(1)\n";
        cout<<"No.....................(2)\n";
        cin>>opcion;

    } while (opcion==1);




    system("pause");
    return 0;
}